<canvas id="myChart" width="400" height="400"></canvas>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Stärke", "Coolheit", "Gayness", "Mag Gon und Killua"],
        datasets: [{
            label: 'Hisoka',
            data: [100, 100, 0, 10000],
            backgroundColor: 'rgba(255, 135, 175, 0.7)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1
        },
        {
            label: 'Gecko Keck',
            data: [0, 0, 100, 1],
            backgroundColor: 'rgba(255, 175, 0, 1)',
            borderColor: 'rgba(255, 175, 0, 1)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>